export const ACTION_REVIEW_SET = '[review] SET';
export const ACTION_REVIEW_DELETE= '[review] DELETE';
export const ACTION_REVIEW_ERROR= '[review] ERROR';
export const ACTION_REVIEW_SUCCESS= '[review] SUCCESS';

export const reviewSetAction = (review) => ({
    type: ACTION_REVIEW_SET,
    payload: review
});

export const reviewErrorAction = (error) => ({
    type: ACTION_REVIEW_ERROR,
    payload: error
});

export const reviewSuccessAction = (success) => ({
    type: ACTION_REVIEW_SUCCESS,
    payload: success
});

export const reviewDeleteAction = (review) => ({
    type: ACTION_REVIEW_DELETE,
    payload: review
});