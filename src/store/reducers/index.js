import {combineReducers} from "redux";
import { reviewReducer } from "./reviewReducer";

const appReducer = combineReducers({
    reviewReducer,
});

export default appReducer;