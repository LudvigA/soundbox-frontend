import {
    ACTION_REVIEW_DELETE,
    ACTION_REVIEW_ERROR,
    ACTION_REVIEW_SET,
    ACTION_REVIEW_SUCCESS,
} from "../actions/reviewActions";

const initialState = {
    review: [],
    error: "",
}

export const reviewReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_REVIEW_SET:
            return{
                ...state,
            };
        case ACTION_REVIEW_ERROR:
            return{
                ...state,
                error: action.payload,
            };
        case ACTION_REVIEW_SUCCESS:
            return {
                ...state,
                review: action.payload,
            };
        case ACTION_REVIEW_DELETE:
            return{
                ...state,
            };
        default:
            return state;
    }
};