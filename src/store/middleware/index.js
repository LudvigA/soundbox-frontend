import {applyMiddleware} from 'redux';
import { reviewMiddleware } from './reviewMiddleware';
export default applyMiddleware(
    reviewMiddleware,
);