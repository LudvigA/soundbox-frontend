import APIService from "../../Services/APIService";

import {
    ACTION_REVIEW_SET,
    reviewErrorAction,
    reviewSuccessAction,
} from "../actions/reviewActions.js";

export const reviewMiddleware =
    ({dispatch}) =>
    (next) =>
    async (action) => {
        next(action);

        if(action.type === ACTION_REVIEW_SET){
            try {
                const reviews = await APIService.getReviews();
                dispatch(reviewSuccessAction(reviews));
            }catch(e) {
                dispatch(reviewErrorAction(action.payload));
            }
        }
    };