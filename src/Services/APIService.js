

const API = "https://localhost:44361/api";

const APIService = {

    getReviews: async () => {

        const requestOptions = {
            method: "GET",
            headers:{
                "Content-Type": "application/json"
            },  
        };
        const apiReviews = await fetch(`${API}/Reviews`, requestOptions);
            return apiReviews.json();
    },

    postReview: async (artistName, songName, reviewText, rating, spotifyLink, reviewerName) => {
        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                artistName: artistName,
                songName: songName,
                reviewText: reviewText,
                rating: rating,
                spotifyLink: spotifyLink,
                reviewerName: reviewerName,
            }),
        };
        const response = await fetch (`${API}/Reviews`, requestOptions);
        return response.json();
    },

    editReview: async (reviewId, reviewText, rating) => {
        const preFetch = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const getReview = await fetch (`${API}/Reviews/${reviewId}`, preFetch)
        const review = await getReview.json();
        const requestOptions = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id: parseInt(reviewId),
                artistName: review.artistName,
                songName: review.songName,
                reviewText: reviewText,
                rating: rating,
                spotifyLink: review.spotifyLink,
                reviewerName: review.reviewerName,
            }),
        };
        await fetch(`${API}/Reviews/${reviewId}`, requestOptions);
    },
    
    deleteReview: async(id) => {
        const requestOptions= {
            method:"DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        };
        try {
            await fetch(`${API}/Reviews/${id}`, requestOptions)
            
        } catch (e) {
            console.log(e.message);
        }
    },
};
export default APIService;