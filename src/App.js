
import './App.css';
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Landingpage from './Pages/Landingpage';
import Profile from './Pages/Profile';

function App() {
 
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Redirect to ="/landingpage"/>
        </Route>
        <Route path="/landingpage" component={Landingpage}/>
        <Route path="/profile" component={Profile}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
