import Header from "../Components/Header/Header"
import Main from "../Components/Main/Main"
import styles from "./Landingpage.module.css"

const Landingpage = () => {
    return(
        <div className={styles.landingpage}>
            <Header></Header>
            <Main></Main>
        </div>
    )
}
export default Landingpage