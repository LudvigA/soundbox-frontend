import Header from "../Components/Header/Header"
import ProfileMain from "../Components/ProfileMain/ProfileMain.js"

const Profile = () => {
    
    return(
        <div>
        <Header/>
        <ProfileMain/>
        </div>
    )
}
export default Profile