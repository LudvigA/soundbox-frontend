import styles from "./Carditem.module.css";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import AOS from "aos";
import spotify from "../../assets/img/spotify.png.png";
import "../../../node_modules/aos/dist/aos.css";
import { reviewSetAction } from "../../store/actions/reviewActions";

const Carditem = () => {
  const { review } = useSelector((state) => state.reviewReducer);
  const [currentreview, setCurrentReview] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    AOS.init({
      duration: 1000,
    });
    setCurrentReview(review);
    dispatch(reviewSetAction());
    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (!isLoading) {
      dispatch(reviewSetAction());
    }
  }, []);

  return (
    <ul className={styles.container}>
      {review.map((r) => {
        return (
          <div key={r.id} id="parent">
            <div
              key={r.id}
              data-aos="fade-in"
              data-aos-anchor="#parent"
              className={styles.cardContent}
            >
              <div key={r.id}>
                <section key={r.id}>
                  <div className={styles.headercontent}>
                    <h2 className={styles.TitleMain}> Artist Name </h2>
                    <p>{r.artistName}</p>
                    <h2 className={styles.TitleMain}>Song name</h2>
                    <p>{r.songName} </p>
                    <h2 className={styles.TitleMain}>Written by</h2>
                    <p>{r.reviewerName}</p>
                  </div>
                  <div className={styles.reviewContent}>
                    <div className={styles.reviewItem}>
                      <h2 className={styles.secondaryTitle}>Review Text</h2>
                      <p>{r.reviewText}</p>
                    </div>
                    <div className={styles.reviewItem}>
                      <h2 className={styles.secondaryTitle}>Rating</h2>
                      <p>{r.rating}/5</p>
                    </div>
                    <div className={styles.reviewItem}>
                      <h2 className={styles.secondaryTitle}>Links</h2>
                      <p className={styles.spotifyLink}>
                        <img
                          className={styles.spotifyIcon}
                          alt="spotify"
                          height="50px"
                          width="50px "
                          src={spotify}
                        />{" "}
                        <a className={styles.spotifyLink} href={r.spotifyLink}>
                          Listen on spotify
                        </a>
                      </p>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        );
      })}
    </ul>
  );
};
export default Carditem;
