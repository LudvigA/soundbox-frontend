import styles from "./Hero.module.css"
import AOS from "aos"
import '../../../node_modules/aos/dist/aos.css';
import { useEffect } from "react";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
const Hero = () => {
    useEffect(() => {
        AOS.init({
          duration : 1500
        });
      }, []);

    return(
        <div className={styles.container}>
            <div className = {styles.heroImage}>
                <h1 data-aos="fade-in" data-aos-delay="100">SoundBox</h1>
                <p data-aos="fade-in" data-aos-delay="200">Welcome to SoundBox, this is a platform where you can post reviews of music you have newly listened to. To start posting create an account and go to your profile page.</p>
                <p>Scroll down to discover new music</p>
                <KeyboardArrowDownIcon className={styles.downIcon}/>
            </div>
        </div>
    )
}
export default Hero