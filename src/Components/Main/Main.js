import Cards from "./Cards";
import Hero from "./Hero";
import styles from "./Main.module.css"
const Main = () => {

    return(
        <div className={styles.maincontainer}>
            <Hero/>
            <Cards></Cards>
        </div>
    )
}
export default Main;