import Carditem from "./Carditem"
import styles from "./Cards.module.css"
const Cards = () => {

    return(
        <div className={styles.cardsmain}>
            <h1>Recent Posts</h1>
        <Carditem className={styles.cards} ></Carditem>
        
        </div>
    )
}
export default Cards