import CreatePost from "./CreatePost";
import Field from "./Field";
import styles from "./ProfileMain.module.css"

const ProfileMain = () => {

    return(
        <div className={styles.ProfileMain}>
            <Field/>
            <CreatePost/>
        </div>
    )
}
export default ProfileMain;