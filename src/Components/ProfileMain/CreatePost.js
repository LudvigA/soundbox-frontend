import styles from "./CreatePost.module.css"
import {useState} from "react"
import APIService from "../../Services/APIService"

const CreatePost = () => {

    const [artistName, setArtistName] = useState("")
    const [songname, setSongName] = useState("")
    const [reviewtext,setReviewText] = useState("")
    const [rating, setRating] = useState("")
    const [spotifylink,setSpotifylink] = useState("")
    const [reviewername, setReveiwerName] = useState("")

    const onArtistnameChange = (e) => {
      setArtistName(e.target.value)
    }
    const onSongNameChange = (e) => {
      setSongName(e.target.value)
    }
    const onReviewTextchange = (e) => {
      setReviewText(e.target.value)
    }
    const onRatingChange = (e) => {
      setRating(e.target.value)
    }
    const onSpotifyLinkChange = (e) => {
      setSpotifylink(e.target.value)
    }
    const onReviewerNameChange = (e) => {
      setReveiwerName(e.target.value)
    }

    const onSubmitReviewClick = async (e) => {
      e.preventDefault();
      await APIService.postReview(artistName,songname,reviewtext,rating,spotifylink,reviewername)
      window.location.reload();
    }

    return(
        <div className={styles.formMain}>
        <form className={styles.formValues}>
        <div className={styles.name}>
          <label>Artist Name</label>
          <input
            maxLength="35"
            required="required"
            placeholder={`Enter artist name`}
            onChange={onArtistnameChange}
          ></input>
        </div>
        <div className={styles.name}>
          <label>Song Name</label>
          <input
            maxLength="30"
            required="required"
            placeholder={`Enter song name`}
            onChange={onSongNameChange}
          ></input>
        </div>
        <div className={styles.review}>
         <label>Review</label>
        <textarea
            className={styles.description}
            required="required"
            placeholder={`Give us your thoughts about the song`}
            onChange={onReviewTextchange}
          ></textarea>
        </div>
        
        <div className={styles.rating}>
            <label>Rating</label>
            <select onChange={onRatingChange}>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            </select>
        </div>
        <div className={styles.link}>
            <label>Spotify link</label>
            <input onChange={onSpotifyLinkChange} pattern="https?://open.spotify.com/.+" placeholder="Enter a valid spotify link"></input>
            <b className={styles.popup}></b>
        </div>
        <div className={styles.reviewer}>
            <label>Reviewer Name</label>
            <input onChange={onReviewerNameChange} placeholder="Enter your name"></input>
        </div>
        <div className={styles.btnDiv}>
          <button
            type="submit"
            className={styles.createBtn}
            onClick={onSubmitReviewClick}
          >
            Create
          </button>
        </div>
      </form>
        </div>
    )
}
export default CreatePost;