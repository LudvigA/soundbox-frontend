import styles from "./Field.module.css"

const Field = () => {

    return(
        <div className ={styles.container}>
            <h1>Profile Page</h1>
            <p>Fill out the form below to post a review</p>
        </div>
    )
}
export default Field;