import styles from "./Header.module.css"
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import FaceIcon from '@mui/icons-material/Face';
import { useHistory } from "react-router-dom";
import logo from "../../assets/img/LogoMusic.png.png"

const Header = () => {
    const history = useHistory();
    const handleProfileClick = () => {
        history.push("/profile");
    }
    const handlePostClick = () => {
        history.push("/landingpage")
    }

    
    return(
<div className={styles.container}>
    <div className={styles.headercontent}>
    <h1><img alt="page logo" height="55px" width="55px" src={logo}></img>SoundBox</h1>
    </div>
    <div className={styles.links}>
    <button  onClick={handlePostClick} className={styles.link}><ContentPasteIcon className={styles.icons}/>Posts</button>
    <button onClick={handleProfileClick}className={styles.link}><FaceIcon className={styles.icons}/>Profile</button>
    </div>
</div>
    )
}
export default Header;